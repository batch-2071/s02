CREATE DATABASE coursebookingdb;
-- USE coursebookingdb;

CREATE TABLE students (
id int NOT NULL AUTO_INCREMENT,
student_name VARCHAR(50) NOT NULL,
PRIMARY KEY (id)
);

CREATE TABLE teachers (
id int NOT NULL AUTO_INCREMENT,
teacher_name VARCHAR(50) NOT NULL,
PRIMARY KEY (id)
);

CREATE TABLE courses (
id int NOT NULL AUTO_INCREMENT,
course_name VARCHAR(50) NOT NULL,
PRIMARY KEY (id),
teacher_id int NOT NULL,
CONSTRAINT fk_courses_course_id
FOREIGN KEY (teacher_id) REFERENCES teachers(id)
ON UPDATE CASCADE
ON DELETE RESTRICT
);

CREATE TABLE student_courses (
id int NOT NULL AUTO_INCREMENT,
PRIMARY KEY (id),
course_id int NOT NULL,
CONSTRAINT fk_student_courses_course_id
FOREIGN KEY (course_id) REFERENCES courses(id)
ON UPDATE CASCADE
ON DELETE RESTRICT,
student_id int NOT NULL,
CONSTRAINT fk_student_courses_student_id
FOREIGN KEY (student_id) REFERENCES students(id)
ON UPDATE CASCADE
ON DELETE RESTRICT
);